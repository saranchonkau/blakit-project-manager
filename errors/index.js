class ApplicationError extends Error {
    constructor(message, status) {
        super();
        Error.captureStackTrace(this, this.constructor);
        this.name = this.constructor.name;
        this.message = message || 'Something went wrong. Please contact with administrator.';
        this.status = status || 500;
    }
}
module.exports.ApplicationError = ApplicationError;

class UserNotFoundError extends ApplicationError {
    constructor(message) {
        super(message || 'No User found.', 404);
    }
}
module.exports.UserNotFoundError = UserNotFoundError;

class ForbiddenOperationError extends ApplicationError {
    constructor(message) {
        super(message || 'You don\'t have enough rights.', 403);
    }
}
module.exports.ForbiddenOperationError = ForbiddenOperationError;

class WrongCredentialsError extends ApplicationError {
    constructor(message) {
        super(message || 'Invalid email or password', 400);
    }
}
module.exports.WrongCredentialsError = WrongCredentialsError;

class NotAuthorizedError extends ApplicationError {
    constructor(message) {
        super(message || 'User must be authorized.', 401);
    }
}
module.exports.NotAuthorizedError = NotAuthorizedError;