const {ApplicationError, ForbiddenOperationError, NotAuthorizedError} = require('../errors');

const passport = require('../passport');

function errorHandler (options) {
    return async function (ctx, next) {
        try {
            await next();
        } catch (error) {
            if (error instanceof ApplicationError) {
                ctx.status = error.status;
                ctx.body = {
                    error: error.message
                };
            } else {
                ctx.status = 500;
                ctx.body = {
                    error: 'Something went wrong. Please contact with administrator.'
                };
            }
            console.log('Error: %s \n Message: %s \n Stack: %s', error.name, error.message, error.stack);
        }
    }
}

function checkAuth (options = { allowed: false }) {
    return async function (ctx, next) {
        await passport.authenticate('jwt', { session: false }, (error, user) => {
            if (user) {
                if (user.isBlocked) {
                    throw new ForbiddenOperationError('Forbidden! User is blocked!');
                }

                if (!options.allowed && user.roleId !== 1) {
                    throw new ForbiddenOperationError();
                }

                ctx.state.user = user;
            } else {
                throw new NotAuthorizedError();
            }
        } )(ctx, next);
        await next();
    };
}

module.exports = {
    errorHandler,
    checkAuth
};