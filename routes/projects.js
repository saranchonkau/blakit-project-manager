const {checkAuth} = require('../middleware');
const projectRepository = require('../components/projects/projectRepository');
const Router = require('koa-router');
const router = new Router();

router
    .get('/', checkAuth(), async (ctx, next) => {
        const projects = await projectRepository.findAll();
        ctx.body = { query: 'GET /projects', results: projects };
    })
    .post('/', checkAuth({ allowed: true }), async (ctx, next) => {
        const newProject = await projectRepository.create(ctx.request.body.name);
        ctx.body = { query: 'GET /projects', results: newProject };
    })
    .get('/:id', checkAuth(), async (ctx, next) => {
        const project = await projectRepository.findById(ctx.params.id);
        ctx.body = { query: 'GET /projects/:id', result: project };
    })
    .put('/:id', checkAuth(), async (ctx, next) => {
        const project = await projectRepository.update({ ...ctx.request.body, id: ctx.params.id });
        ctx.body = { query: 'PUT /projects/:id', result: project };
    })
    .delete('/:id', checkAuth(), async (ctx, next) => {
        const rowsCount = await projectRepository.removeById(ctx.params.id);
        ctx.body = { query: 'DELETE /projects/:id', result: rowsCount };
    });

module.exports = router;