const {UserNotFoundError} = require('../errors');
const Router = require('koa-router');
const config = require("config");
const jwt = require("jsonwebtoken");
const router = new Router();
const passport = require('../passport');

router.post('/', async (ctx, next) => {
    await passport.authenticate('local', function (err, user) {
        if (!user) {
            throw new UserNotFoundError();
        } else {
            const payload = {
                id: user.id,
                email: user.email,
                roleId: user.roleId,
                isBlocked: user.isBlocked
            };
            const token = jwt.sign(payload, config.get('jwtSecret'));
            ctx.body = { user: user.name, token: `Bearer ${token}` };
        }
    })(ctx, next);
});


module.exports = router;