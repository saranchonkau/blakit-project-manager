const Router = require('koa-router');
const config = require('config');
const projectRouter = require('./projects');
const userRouter = require('./users');
const authRouter = require('./login');

const router = new Router();

router.prefix(config.get('server.contextPath'));

router.use('/projects', projectRouter.routes());
router.use('/users', userRouter.routes());
router.use('/login', authRouter.routes());

module.exports = router;