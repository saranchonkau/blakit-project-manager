const {checkAuth} = require('../middleware');
const userRepository = require('../components/users/userRepository');
const Router = require('koa-router');
const router = new Router();

router
    .post('/', checkAuth(), async (ctx, next) => {
        ctx.body = await userRepository.create(ctx.request.body);
    })
    .delete('/:id', checkAuth(), async (ctx, next) => {
        ctx.body = await userRepository.deleteById(ctx.params.id);
    });

module.exports = router;