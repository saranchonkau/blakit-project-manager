const {WrongCredentialsError} = require('./errors');

const passport = require('koa-passport');
const LocalStrategy = require('passport-local');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const userRepository = require('./components/users/userRepository');
const config = require("config");
const { checkPassword } = require("./utils/auth");

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.get('jwtSecret')
};

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        session: false
    },
    async (email, password, done) => {
        try {
            const user = await userRepository.findByEmail(email);
            if (!user) throw new WrongCredentialsError();

            const isValidPassword = await checkPassword(password, user.passwordHash);
            if (!isValidPassword) throw new WrongCredentialsError();

            done(null, user);
        } catch (error) {
            done(error);
        }
    })
);

passport.use(new JwtStrategy(
    jwtOptions,
    async (jwtPayload, done) => {
        try {
            if (jwtPayload) {
                done(null, jwtPayload);
            } else {
                done(null, false);
            }
        } catch (error) {
            done(error)
        }
    })
);

module.exports = passport;
