const Roles = {
    ADMINISTRATOR: { id: 1, name: 'ADMINISTRATOR' },
    MODERATOR: { id: 2, name: 'MODERATOR' }
};

const DEFAULT_DELETION_TOKEN = '00000000-0000-0000-0000-000000000000';

module.exports = {
    Roles,
    DEFAULT_DELETION_TOKEN
};