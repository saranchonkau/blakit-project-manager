const Project = require('./Project');

function findById(projectId) {
    return Project.findById(projectId);
}

function findAll() {
    return Project.findAll();
}

function create(projectName) {
    return Project.create({ name: projectName });
}

function update({ id, name }) {
    return Project.update({ name }, { where: { id } });
}

function removeById(projectId) {
    return Project.destroy({ where: { id: projectId } });
}

module.exports = {
    findById,
    findAll,
    create,
    removeById,
    update
};