const { DEFAULT_DELETION_TOKEN } = require('../../constants');
const User = require('./User');
const uuidv4 = require('uuid/v4');

function findById(userId) {
    return User.findById(userId);
}

function deleteById(userId) {
    return User.update({ deletion_token: uuidv4() }, { where: { id: userId } });
}

function findByEmail(email) {
    return User.findOne({ where: {email, deletion_token: DEFAULT_DELETION_TOKEN} });
}

function findAll() {
    return User.findAll();
}

function create(user) {
    return User.create(user);
}

function update(user) {
    return User.update(user, { where: { id: user.id } });
}

// function removeById(userId) {
//     return User.destroy({ where: { id: userId } });
// }

module.exports = {
    findById,
    findByEmail,
    findAll,
    create,
    deleteById,
    // removeById,
    update
};