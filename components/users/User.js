const Sequelize = require('sequelize');
const { sequelize } = require('../../db');
const authUtils = require('../../utils/auth');
const Role = require('../roles/Role');
const { DEFAULT_DELETION_TOKEN } = require('../../constants');

const User = sequelize.define('users', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING(45),
        allowNull: false
    },
    email: {
        type: Sequelize.STRING(45),
        allowNull: false,
        unique: 'email_deletion_token_index'
    },
    passwordHash: {
        type: Sequelize.CHAR(60),
        allowNull: false,
        field: 'password_hash'
    },
    password: Sequelize.VIRTUAL,
    roleId: {
        type: Sequelize.TINYINT,
        allowNull: false,
        defaultValue: 2,
        field: 'role_id',
        references: {
            model: Role,
            key: 'id'
        }
    },
    isBlocked: {
        type: Sequelize.BOOLEAN,
        field: 'is_blocked',
        defaultValue: false
    },
    deletion_token: {
        type: Sequelize.CHAR(36),
        unique: 'email_deletion_token_index',
        defaultValue: DEFAULT_DELETION_TOKEN
    }
}, {
    timestamps: false
});

User.beforeCreate(async (user, options) => {
    user.passwordHash = await authUtils.cryptPassword(user.password);
});

module.exports = User;