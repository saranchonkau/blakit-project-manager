const Sequelize = require('sequelize');
const { sequelize } = require('../../db');

const Role = sequelize.define('roles', {
    id: {
        type: Sequelize.TINYINT,
        primaryKey: true,
        allowNull: false
    },
    name: {
        type: Sequelize.STRING(20),
        allowNull: false,
        unique: true
    }
}, {
    timestamps: false
});

module.exports = Role;