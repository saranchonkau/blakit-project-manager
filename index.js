const Koa = require('koa');
const app = new Koa();

const { errorHandler } = require('./middleware');
const config = require('config');
const logger = require('koa-logger');
const bodyParser = require('koa-bodyparser');
const passport = require('./passport');
const appRouter = require('./routes');
const { initializeDatabase } = require('./utils/db');

app.use(errorHandler());
app.use(logger());
app.use(bodyParser());
app.use(passport.initialize());
app.use(appRouter.routes());
app.use(appRouter.allowedMethods());

async function initApp () {
    try {
        await initializeDatabase();
        app.listen(config.get('server.port'), () => {
            console.log('Example app listening on port 3000!');
        });
    } catch (error) {
        console.error(error);
    }
}

initApp();

