const Sequelize = require('sequelize');
const config = require('config');
const sequelize  = new Sequelize({
    dialect: 'mysql',
    host: config.get('db.host'),
    username: config.get('db.user'),
    password: config.get('db.password'),
    database: config.get('db.database')
});


module.exports.sequelize = sequelize;