const config = require('config');
const bcrypt = require('bcrypt');

function cryptPassword (password) {
    console.log('CRYPT PASSWORD: [%s]', password);
    return bcrypt.hash(password, config.get('saltRounds'));
}

function checkPassword(plainPassword, passwordHash) {
    console.log('CHECK PASSWORD: [%s] , AHD HASH: [%s]', plainPassword, passwordHash);
    return bcrypt.compare(plainPassword, passwordHash);
}

module.exports = {
    cryptPassword,
    checkPassword
};