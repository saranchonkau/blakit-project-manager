const { Roles, DEFAULT_DELETION_TOKEN } = require('../constants');
const { sequelize } = require('../db');
const Project = require('../components/projects/Project');
const User = require('../components/users/User');
const Role = require('../components/roles/Role');

const PROJECTS = [
    { name: 'Sooma Doctors' },
    { name: 'Meddy' },
    { name: 'Domino' }
];

const ROLES = [
    Roles.ADMINISTRATOR,
    Roles.MODERATOR
];

const USERS = [
    {
        name: 'Ivan Saranchonkau',
        email: 'ivan.saranchenkov@gmail.com',
        password: 'Thebest',
        passwordHash: '$2b$10$AkXOmACkozleXHOPbfWH8ukoDYZtpLk9aFVtYeDnjPqOXyumKbo6.',
        isBlocked: false,
        roleId: Roles.ADMINISTRATOR.id,
        deletion_token: DEFAULT_DELETION_TOKEN
    }
];

async function initializeDatabase() {
    await sequelize.drop();
    await sequelize.sync();
    await Role.bulkCreate(ROLES);
    await Project.bulkCreate(PROJECTS);
    await User.bulkCreate(USERS);
}

module.exports = {
    initializeDatabase
};
